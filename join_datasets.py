from pathlib import Path
import pickle
from rich import print
import pandas as pd


def get_dataframe_extended():
    path = Path.cwd() / "all_data/dataset_extended.data"
    databytes = path.read_bytes()
    df = pickle.loads(databytes)
    return df["df"]


def get_dataframe_extended_ohv():
    path = Path.cwd() / "all_data/dataset_extended_ohv.data"
    databytes = path.read_bytes()
    df = pickle.loads(databytes)
    return df


def get_dataframe_extended_nlp():
    path = Path.cwd() / "all_data/dataset_nlp_description.data"
    databytes = path.read_bytes()
    df = pickle.loads(databytes)
    return df


def save_df(df):
    databytes = pickle.dumps(df)
    path = Path.cwd() / "all_data/dataset_total_extended.data"
    path.write_bytes(databytes)


def join_datasets():
    path_nlp = Path.cwd() / "all_data/dataset_nlp_description.data"
    databytes = path_nlp.read_bytes()
    dataset_nlp = pickle.loads(databytes)
    bag = dataset_nlp["bag"]
    dataset = []
    for t, d in dataset_nlp["nlp"]:
        del d["lemmas"]
        data = dict(zip(bag, d["vector"]))
        del d["vector"]
        d.update(data)
        dataset.append(d)
    print("Creando dataframe")
    df = pd.DataFrame(dataset)
    print("NLP", df.shape, df.columns)
    df_extended = get_dataframe_extended()
    df_extended_ohv = get_dataframe_extended_ohv()
    df_final = pd.concat(
        (df_extended, df_extended_ohv, df), axis=1)
    print(df_final.shape)
    print(df_final.head())
    save_df(df_final)


join_datasets()
