from model_classifier import (
    GamedataColumnTransformer,
    ConvertDatetime2Timestamp,
    OnlyNumericTransformer,
    DropZeroColumnTransformer,
    ExpandDatetimeColumnTransformer)
from analisis_datos import load_train, get_mapa, FORMAT, load_test
from model_classifier import classifier_separate_x_y
from read_total_extended import get_dataframe_test_extended
from pathlib import Path
import pickle

# from custom_transformer import GamedataColumnTransformer


def read_model(path):
    databytes = path.read_bytes()
    datamodel = pickle.loads(databytes)
    return datamodel


models = []
path = Path.cwd/"models" / \
    "best_models_do_prestigio_classifiers_2022-12-16T12:29:51.032389.data"
model = read_model(path)
# for path in Path("models").rglob("*.data"):
#     datamodel = read_model(path)
#     #print(path.name, datamodel["score"])
#     models.append(datamodel)

# best_model = max(models, key=lambda e: e["score"])

dataset_test = get_dataframe_test_extended()

dataset = load_train()
mapa, class_fields, separate = get_mapa(dataset)

reverse_map = {}
for n, v in mapa["uniques_rating"].items():
    print(n, v, v.toint)
    reverse_map[v.toint] = n

print(reverse_map)

print(best_model.keys())
#m = best_model["estimators"]
m = model["model"]
# for c in dataset_test.columns:
#     print(c)

X_mod, y_mod = classifier_separate_x_y(dataset_test)

print("Input::", X_mod.shape)

y_pred = m.predict(X_mod)

print(y_pred)

y_save = [reverse_map[v] for v in y_pred]

path = Path("predictions_clf.txt")

with path.open("w", encoding="utf8") as f:
    for v in y_save:
        f.write(f"{v}\n")

print("Saved", path)
