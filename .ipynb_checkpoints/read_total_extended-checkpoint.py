from pathlib import Path
import pickle
from rich import print
import pandas as pd


def get_dataframe_extended():
    path = Path.cwd() / "all_data/dataset_extended_final.data"
    databytes = path.read_bytes()
    df = pickle.loads(databytes)
    return df


def get_dataframe_test_extended():
    path = Path.cwd() / "all_data/dataset_test_extended_final.data"
    databytes = path.read_bytes()
    df = pickle.loads(databytes)
    return df


if __name__ == "__main__":
    df = get_dataframe_extended()
    print(df.sample(n=5))
    print(df.columns)
