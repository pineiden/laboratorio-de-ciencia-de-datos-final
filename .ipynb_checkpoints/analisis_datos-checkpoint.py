import typer
import dill
from functools import partial
from slugify import slugify
from statistics import mean, stdev
from datetime import datetime, timedelta
from collections import Counter
import pickle
from pathlib import Path
from rich import print
from one_hot_vector import OneHotVector
import numpy as np
from functools import reduce
import pandas as pd

# trabajo por hilos
import asyncio
import stanza
import json
import concurrent.futures
import spacy_stanza
from normalizar_texto import normalize

FORMAT = "%Y-%m-%d"


def read_file(path):
    print("Path", path)
    if path.exists():
        databytes = path.read_bytes()
        return pickle.loads(databytes)
    else:
        return pd.DataFrame()


def load_train():
    train = Path.cwd() / "all_data/train.pickle"
    dataset = read_file(train)
    return dataset

def load_test():
    train = Path.cwd() / "all_data/test.pickle"
    dataset = read_file(train)
    return dataset


def get_one_hot(nombres, fields):
    lista = nombres.split(";")
    vector = np.array([1 if k in lista else 0 for k in fields])
    one_hot_vector = OneHotVector(names=fields, value=vector)
    return one_hot_vector


def separate_fields(lista):
    field_set = set()

    for f in lista:
        field_set |= set(f.split(";"))

    field_list = list(field_set)
    field_list.sort()
    return field_list


def process_field(field, uniques):
    """
    genera un registro de cada valor en campo a un onehot
    """
    field_list = uniques[field]
    map_field = {g: OneHotVector.create(field_list, i)
                 for i, g in enumerate(field_list)}
    return map_field

    # developers = set()
    # for dev in uniques["developer"]:
    #     developers |= set(dev.split(";"))

    # print(len(developers), len(uniques["developer"]))

    # publishers = set()
    # for dev in uniques["publisher"]:
    #     publishers |= set(dev.split(";"))

    # print(len(publishers), len(uniques["publisher"]))


def process_genres(uniques):
    map_field = process_field("genres", uniques)
    return map_field
# check categories


def process_categories(uniques):
    map_field = process_field("categories", uniques)
    return map_field

# check tags


def process_tags(uniques):
    map_field = process_field("tags", uniques)
    return map_field


def process_platforms(uniques):
    map_field = process_field("platforms", uniques)
    return map_field


def process_rating(uniques):
    map_field = process_field("rating", uniques)
    return map_field


def col_clean(column):
    return column.drop_duplicates().sort_values()


def generate_uniques_map(dataset, class_fields, separate):
    uniques = {}
    for field in class_fields:
        column = dataset[field]
        if field in separate:
            uniques[field] = separate_fields(col_clean(column))
        else:
            uniques[field] = col_clean(column)
    # Create one_hot encoder
    mapa = dict(
        uniques_genres=process_genres(uniques),
        uniques_categories=process_categories(uniques),
        uniques_tags=process_tags(uniques),
        uniques_platforms=process_platforms(uniques),
        uniques_rating=process_rating(uniques)
    )
    return mapa


"""
El peso de cada columna se entrena en el aprendizaje automatizado.

Crear campos:
El juego en sí, comparado con el resto
El juego en sí, comparado con el campo X

Calcular hasta la fecha de cada juego:

General:
X TJ = #Total Juegos + rating
X TV = #Total Ventas
X Por developer + rating
X Por publisher + rating

Sub-filtro: gn = #genero juegos
TJ = #Total Juegos + rating
TV = #Total Ventas
Por developer + rating
Por publisher + rating

Sub-filtro: gn = #genero juegos
TJ = #Total Juegos + rating
TV = #Total Ventas
Por developer + rating
Por publisher + rating

Sub-filtro: tj = #tags juegos
TJ = #Total Juegos + rating
TV = #Total Ventas
Por developer + rating
Por publisher + rating

Sub-filtro: cj = #categorias juegos

Sub-filtro: vj = #total ventas (genero, tags, categorias) /
Este valor debe integrar todas los generos/tags, etc para comparar
hasta el momento

Sub-filtro: pj = #cantidad plataformas por juego/medio

Sub-filtro: Frecuencia de plataforma:
Sub-filtro: pfj = #plataforma mas frecuente
Sub-filtro: pmfj = #plataforma menos frecuente
Sub-filtro:  pmfmj = #plataforma frecuencai media

Sub-filtro:  Plataforma y género
p_genero = #plataforma_genero (1 col por plataforma)
p_categoria = #plataforma_categoria (1 col por plataforma)
p_tags = #plataforma_tags (1 col por plataforma)
p_ventas_genero = #plataforma_venta_genero (1 col por plataforma)
p_ventas_categoria = #plataforma_venta_genero (1 col por plataforma)
p_ventas_tags = #plataforma_venta_genero (1 col por plataforma)

Sub-filtro: Idioma, plataforma y genero
idioma_ingles = #juegos_ingles

Sub-filtro: Restricciones de edad
rage_genero = #juegos_generos_requiered_age
rage_categoria = #juegos_generos_requiered_age
rage_tags = #juegos_generos_requiered_age
rage_genero_venta = #juegos_generos_requiered_age
rage_categoria_ventas = #juegos_generos_requiered_age
rage_tags_ventas = #juegos_generos_requiered_age
rage_genero_venta = #juegos_generos_requiered_age
rage_dev_categoria_ventas = #juegos_generos_requiered_age
rage_dev_tags_ventas = #juegos_generos_requiered_age
rage_dev_genero_venta = #juegos_generos_requiered_age
rage_dev_categoria_ventas = #juegos_generos_requiered_age
rage_pub_tags_ventas = #juegos_generos_requiered_age
rage_pub_categoria_ventas = #juegos_generos_requiered_age
rage_pub_tags_ventas = #juegos_generos_requiered_age
rage_pub_genero_venta = #juegos_generos_requiered_age
rage_pub_categoria_ventas = #juegos_generos_requiered_age
rage_pub_tags_ventas = #juegos_generos_requiered_age

# Hacer lo mismo con price :: valor del juego
# Obtener el valor medio del juego por cada otra clase
# Obtener el valor desv estandar del juego por cada otra clase
# Hacer lo mismo con average_playtime :: tiempo jugado
# Obtener el valor medio del juego por cada otra clase
# Obtener el valor desv estandar del juego por cada otra clase

NOTA:: obtener los campos por el total de tiempo, por periodos
anuales, mensuales y temporadas

Esto servirá para caracterizar al 'developer' y al 'publisher'
que tendrán un valor de 'prestigio' a como resulten los juegos
anteriors y lo que ya exista.
"""
# Determinación del prestigio de cada developer y publisher


Numeric = (int, float)


def generate_periods(years, months, seasons, names):
    """
    opts: add extra filters {field: categories_list}


    """
    dataset = {n: [] for n in names}
    # years
    for year in years:
        if year < years[-1]:
            dyear = datetime(year=year, month=1, day=1)
            endyear = datetime(year=year+1, month=1, day=1)
            pair = (dyear, endyear)
            dataset["year"].append(pair)
        for month in months[:-1]:
            dym = datetime(year=year, month=month, day=1)
            lym = datetime(year=year, month=month+1, day=1) - \
                timedelta(seconds=1)
            pair = (dym, lym)
            dataset["month"].append(pair)
        for season, limits in seasons.items():
            start, end = limits
            ds0 = datetime(year, month=start, day=1)
            ds1 = datetime(year, month=end+1, day=1) - timedelta(seconds=1)
            pair = (ds0, ds1)
            dataset[f"season_{season}"].append(pair)
    return dataset


def clean(word):
    letras = [chr(i) for i in range(ord('a'), ord('z')+1)] + \
        [f"{i}" for i in range(10)]
    return "".join([w for w in word if w in letras])


def op_words(doc):
    sentence_data = {
        "chars": len(doc.text),
        "len": len(doc.sentences),
        "words": sum([len(s.words) for s in doc.sentences]),
        "verbs": 0,
        "noun": 0,
        "adj": 0,
        "adv": 0,
        "others": 0,
        "numeric": 0,
        "prons": 0,
        "conj": 0,
        "lemmas": set()
    }

    words = []
    omit = {"PUNCT", "STOP", "DET", "ADP"}
    for s in doc.sentences:
        for w in s.words:
            if w.upos not in omit:
                # contar verbs
                if w.upos == "VERB":
                    sentence_data["verbs"] += 1
                # contar nounes
                elif w.upos == "NOUN":
                    sentence_data["noun"] += 1
                # contar adj
                elif w.upos == "ADJ":
                    sentence_data["adj"] += 1
                elif w.upos == "ADV":
                    sentence_data["adv"] += 1
                elif w.upos == "NUM":
                    sentence_data["numeric"] += 1
                elif w.upos == "PRON":
                    sentence_data["prons"] += 1
                elif w.upos in ("CCONJ", "SCONJ"):
                    sentence_data["conj"] += 1
                else:
                    sentence_data["others"] += 1

                # countar adv
                if w.lemma:
                    if len(w.lemma) > 2 and "&" not in w.lemma:
                        sentence_data["lemmas"].add(clean(w.lemma))
                        words.append(len(w.text))

    if len(words) > 0:
        sentence_data["mean_len_words"] = np.mean(words)
        if len(words) > 1:
            sentence_data["std_len_words"] = np.std(words)
        else:
            sentence_data["std_len_words"] = 0
    else:
        sentence_data["mean_len_words"] = 0
    return sentence_data


def bag_words(dataset):
    wordset = [b["lemmas"] for index, name, a, b in dataset]
    words = list(reduce(lambda a, b: a | b, wordset))
    words.sort()
    return words


def union_one_hot(vector):
    return reduce(lambda a, b: a | b, vector)


def analisis_descripcion(dataset):
    stanza.download("en")
    nlp = stanza.Pipeline("en")
    start = datetime.utcnow()
    print(f"Iniciando NLP short_description {start}")

    newdataset = []
    try:
        for index, data in dataset.iterrows():
            sentence = normalize(data.short_description)
            name = data["name"]
            analisis = nlp(sentence)
            mineria = op_words(analisis)
            newdataset.append((index, name, sentence, mineria))
    except Exception as e:
        print("Error al procesar pipeline nlp")
        raise e
    end = datetime.utcnow()
    secs = (end-start).total_seconds()
    bag = bag_words(newdataset)
    n = len(bag)
    # ira bien tirarle onhehot?=
    vectores = []
    vect_dict = {}
    rbag = bag[::-1]
    for index, name, k, mineria in newdataset:
        lista = mineria["lemmas"]
        vector = np.array(
            [1 if b in lista else 0 for i, b in enumerate(rbag)])
        vectores.append(vector)
        mineria["vector"] = vector

    return newdataset, rbag


def filtrar(df_sorted, data, index, periodos, pipeline, class_fields):
    # analisis de 'short_description'
    # se añade
    newdata = {}
    # si field en class_fields
    # filtrar
    for name, periods in periodos.items():
        releases_dict = {i: [] for i, p in enumerate(pipeline)}
        for start, end in periods:
            period_releases = df_sorted[(start <= df_sorted.release_date) &
                                        (df_sorted.release_date <
                                         end)]

            # this is a filter step that consider every item in
            # pipeline

            for i, (field, key, callback, opts) in enumerate(pipeline):
                copy_period_releases = period_releases.copy(deep=True)
                is_class = field in class_fields
                if is_class:
                    # filter by value
                    value = data[field]
                    copy_period_releases = copy_period_releases[copy_period_releases[field] == value]

                if opts:
                    # por cada campo a filtrar, obtener
                    # lista de valores que considera
                    values = set(data[field].split(";"))
                    check = copy_period_releases.apply(lambda
                                                       row: len(set(row[field].split(";"))
                                                                & values) > 0,
                                                       axis=1)
                    if not check.empty:
                        copy_period_releases = copy_period_releases[check]

                # period_releases is a dataframe filtered, callback
                # operate over this
                # count quantity of games in this period
                releases_dict[i].append(callback(period_releases))
            # valor medio del juegos liberados en ese periodo

        # insert in field
        # print("This period", name, len(releases), sum(releases))
        for i, releases in releases_dict.items():
            field, key, callback, opts = pipeline[i]
            releases = [r for r in releases if isinstance(r, Numeric)
                        and np.isfinite(r)]
            if releases:
                if not opts:
                    newdata[f"{field}_{key}_med_{name}"] = mean(releases)
                    if len(releases) > 2:
                        newdata[f"{field}_{key}_std_{name}"] = stdev(
                            releases)
                else:
                    """si hay opts"""
                    for fopts, values in opts.items():
                        for fop in values:
                            fop = slugify(fop)
                            c1 = f"{field}_{fop}_{key}_med_{name}"
                            c2 = f"{field}_{fop}_{key}_std_{name}"
                            m = mean(releases)
                            newdata[c1] = m
                            if len(releases) > 2:
                                newdata[c2] = stdev(releases)
    return newdata


def callback_notas(period_releases):
    notas = {k: i for i, k in enumerate(
        ('Negative', 'Mixed', 'Mostly Positive', 'Positive', 'Very Positive'))}

    lista = np.array([notas[r] for r in
                      period_releases.rating])
    if len(lista) > 0:
        return np.mean(lista)


def s2d(s):
    return datetime.strptime(s, FORMAT)


def crear_periodos(release_date, start_date):
    seasons = {
        "invierno": (12, 3),
        "verano": (7, 8),
        "otoño": (9, 11),
        "primavera": (4, 6)
    }
    season_fields = [f"season_{s}" for s in seasons]
    names = ["year", "month", *season_fields]

    months = [i for i in range(1, 13)]

    # release_date = s2d(release_date)
    years = [y for y in range(start_date.year, release_date.year)]
    periodos = generate_periods(years, months, seasons, names)

    return periodos


"""
Las siguientes funciones se definen en base a 'field' que determina el
primer referente.
Luego el 'key' determinará el valor a extraer del data en la fila específica
El callback será la función que extraerá el valor final tomando en
cuenta que se le entrega el dataframe filtrado
"""


def tiempo_juego(field, opts={}):
    key = "average_playtime"

    def callback_time(x):
        if not x[key].empty:
            return np.mean(x[key])

    item = (field, key, callback_time, {})
    return item


def apply_periodos(opts={}):  # ):

    # med y dev
    # anual
    # mensual
    # semestral
    # temporada : invierno, verano, etc
    # now for every period, calculate media y std

    pipeline = [
        ("rating_total", "cantidad", callback_notas, {}),
    ]

    # newdata = filtrar(df_sorted, data, "total", index, periodos, key="cantidad",
    #                   callback=lambda x: x.shape[0])
    return pipeline


def total_ventas(opts={}):
    field = "estimated_sells"
    key = "ventas"

    def callback(x):
        return sum(x["estimated_sells"])

    pipeline = [
        (field, key, callback, {})

    ]
    return pipeline


def filtrar_por_developer(opts={}):
    field = "developer"

    # ver cantidad por dev
    key_1 = "cantidad"

    def callback_1(x): return x.shape[0]
    # ver total ventas por dev

    def callback_2(x):
        return sum(x["estimated_sells"])

    key_2 = "ventas"
    key_3 = "rating"

    pipeline = [
        tiempo_juego(field, opts),
        (field, key_1, callback_1, opts),
        (field, key_2, callback_2, opts),
        (field, key_2, callback_notas, opts)
    ]

    # filtrar por 'developer'
    # sacar el total
    # sacar ventas

    return pipeline


def filtrar_por_publisher(opts={}):
    field = "publisher"
    # ver cantidad por dev
    key_1 = "cantidad"
    key_2 = "ventas"
    key_3 = "rating"

    def callback_1(x): return x.shape[0]
    # ver total ventas por dev

    def callback_2(x):
        return sum(x["estimated_sells"])

    pipeline = [
        tiempo_juego(field, opts),
        (field, key_1, callback_1, opts),
        (field, key_2, callback_2, opts),
        (field, key_2, callback_notas, opts)
    ]

    return pipeline
    # filtrar(data, "developer", index, periodos, key="rating",
    #         callback=callback_notas)


def filtrar_genres(mapa, opts={}):
    field = "genres"
    uniques_genres = mapa["uniques_genres"]
    opts = {field: uniques_genres.keys()}

    pipeline = apply_periodos(opts)
    pipeline += total_ventas(opts)
    pipeline += filtrar_por_developer(opts)
    pipeline += filtrar_por_publisher(opts)

    return pipeline


def filtrar_categories(mapa, opts={}):
    field = "categories"
    uniques_categories = mapa["uniques_categories"]

    opts = {field: uniques_categories.keys()}

    pipeline = apply_periodos(opts)
    pipeline += total_ventas(opts)
    pipeline += filtrar_por_developer(opts)
    pipeline += filtrar_por_publisher(opts)

    return pipeline


def filtrar_tags(mapa, opts={}):
    field = "tags"
    uniques_tags = mapa["uniques_tags"]

    opts = {field: uniques_tags.keys()}

    pipeline = apply_periodos(opts)
    pipeline += total_ventas(opts)
    pipeline += filtrar_por_developer(opts)
    pipeline += filtrar_por_publisher(opts)

    return pipeline


def filtrar_platforms(mapa, opts={}):
    field = "platforms"
    uniques_platforms = mapa["uniques_platforms"]

    opts = {field: uniques_platforms.keys()}

    pipeline = apply_periodos(opts)
    pipeline += total_ventas(opts)
    pipeline += filtrar_por_developer(opts)
    pipeline += filtrar_por_publisher(opts)

    return pipeline

# esto podría ser un decorador ::


async def process_filtrar(until_release, data, index, periodos,
                          pipeline, class_fields):
    start = datetime.utcnow()
    results = filtrar(until_release,
                      data,
                      index,
                      periodos,
                      pipeline,
                      class_fields)
    end = datetime.utcnow()
    secs = (end-start).total_seconds()
    name = data["name"]
    rdate = data["release_date"]
    print(f"STATUS::{name};{rdate};{secs}")
    results["index"] = index
    results["name"] = name
    return results


def run_batch(dataset, mapa, class_fields):
    """
    dataset filtered with the trained data and ordered
    """
    results = []
    tasks = []
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    print("Loop", loop)
    pipeline = apply_periodos()
    pipeline += total_ventas()
    pipeline += filtrar_por_developer()
    pipeline += filtrar_por_publisher()
    pipeline += filtrar_genres(mapa)
    pipeline += filtrar_categories(mapa)
    pipeline += filtrar_tags(mapa)
    pipeline += filtrar_platforms(mapa)

    for args in dataset:
        args.append(pipeline)
        args.append(class_fields)
        task = process_filtrar(*args)
        tasks.append(task)
    if not loop.is_running():
        results = loop.run_until_complete(asyncio.gather(*tasks))
        print("Resultados gather:::", len(results))
        return results


def distribuir(lista, n=63):
    position = 0
    dataset = {i: [] for i in range(n)}
    for w in lista:
        dataset[position].append(w)
        position += 1
        if position >= n:
            position = 0
    return dataset


async def obtener_prestigio(df_sorted,
                            start_date,
                            mapa,
                            class_fields,
                            train=True):
    # pasar a datetime
    df_sorted["release_date"] = df_sorted["release_date"].apply(s2d)

    # set total games at column
    if "total_games" not in df_sorted.columns:
        df_sorted["total_games"] = np.nan

    # set total games by catgory associated to this game
    start = datetime.utcnow()
    dataset = []
    registro = []

    loop = asyncio.get_running_loop()

    results = []
    total_games_list = []

    # en caso de hacer train, ambos df seran el mismo
    dataset_args = []
    select = 1 if train else 0
    no_train_filter = df_sorted["train"] == select
    df_sorted_select = df_sorted.where(no_train_filter)
    train_filter = df_sorted["train"] == 1
    df_sorted_historical = df_sorted.where(train_filter)

    # se itera sobre el df de interés
    for i, (index, data) in enumerate(df_sorted_select.iterrows()):
        # iterar secuencialmente todos los datos
        # considerando los datos de prueba
        release_date = data.release_date
        periodos = crear_periodos(release_date, start_date)
        # seleccionar solo los datos de entrenamiento
        # el df_sorted_historical tendra solo datos de train
        until_release = df_sorted_historical[df_sorted.release_date <
                                             release_date].where(train_filter)
        # total total until now
        total_games = until_release.shape[0]
        # apply periodios
        newdata = [until_release, data, index, periodos]
        dataset_args.append(newdata)
        total_games_list.append({"total_games": total_games})

    # se distribuye en multiprocesos la seleccion de nuevos datos
    # el tamaño de dataset_args será el de los nuevos datos
    dataset_distribuido = distribuir(dataset_args)
    # pipeline += mas pipelines
    with concurrent.futures.ProcessPoolExecutor(max_workers=63) as executor:
        try:
            for k, v in dataset_distribuido.items():
                if v:
                    print(f"Set group {k}")
                    result = loop.run_in_executor(executor,
                                                  partial(run_batch,
                                                          v,
                                                          mapa,
                                                          class_fields))
                    results.append(result)
            print("Iniciando procesamiento de datos")
            final_results = [await r for r in results]
            fr = []
            for rs in final_results:
                fr += rs
            print("Final results, total campos:", len(fr))
            return fr, total_games_list
            # add the newdata for every step
        except Exception as e:
            print("Error al filtrar data")
            raise e


def process_data(dataset_sorted, start_date, mapa, class_fields, train=True):
    dataset, total_games_list = asyncio.run(
        obtener_prestigio(dataset_sorted, start_date, mapa,
                          class_fields, train=train))
    df_cols = pd.DataFrame(dataset)
    df_total = pd.DataFrame(total_games_list)
    df_sorted_1 = pd.concat((df_cols, df_total), axis=1)
    # se retorna un dataset ordenado de los datos nuevos
    # si es test, el tamaño será la candiad de filas test
    return df_sorted_1


def save_data_df(df_sorted_1):
    databytes = pickle.dumps(df_sorted_1)
    mode = "train" if train else "test"
    path = Path.cwd() / f"all_data/dataset_extended_{mode}.data"
    path.write_bytes(databytes)
    return df_sorted_1


def do_nlp(dataset_sorted, train=True):
    select = 1 if train else 0
    no_train_filter = dataset_sorted["train"] == select
    dataset_select = dataset_sorted.where(no_train_filter)
    description, rbag = analisis_descripcion(
        dataset_select[["name", "short_description"]])
    return description, rbag


def build_nlp_dataframe(description, bag):
    dataset = []
    for index, name, sentence, d in description:
        del d["lemmas"]
        data = dict(zip([f"shd_{field}" for field in bag], d["vector"]))
        del d["vector"]
        item = dict(index=index, name=name, **d, **data)
        dataset.append(item)
    df = pd.DataFrame(dataset)
    return df


def save_nlp(description, rbag, train=True):
    databytes = pickle.dumps({"nlp": description, "bag": rbag})
    mode = "train" if train else "test"
    path = Path.cwd() / f"all_data/dataset_nlp_description_{mode}.data"
    path.write_bytes(databytes)
    return path


app = typer.Typer()


def create_one_hot_alias(dataset, mapa, train=True):
    select = 1 if train else 0
    no_train_filter = dataset["train"] == select
    dataset_select = dataset.where(no_train_filter)
    newdataset = []
    for i, (index, data) in enumerate(dataset_select.iterrows()):
        release_date = data.release_date
        name = data["name"]
        item = {"name": name, "release_date": release_date}

        for field, unique in mapa.items():
            dfield = field.replace("uniques_", "")
            values = data[dfield].split(";")
            one_hots = [unique.get(v) for v in values]
            all_oh = reduce(lambda a, b: a | b, one_hots)
            fitem = {slugify(f"{field}_{k}"): v for k,
                     v in all_oh.todict().items()}
            item.update(fitem)
        newdataset.append(item)
    df = pd.DataFrame(newdataset)
    # retorna un dataframe cuya cantidad de filaes es
    # la de entrada para train (si se entrena)
    # o la de test si se hace test
    return df


def df_one_hots(dataset, mapa, train=True):
    df = create_one_hot_alias(dataset, mapa, train=train)
    mode = "train"
    if not train:
        mode = "test"
    path = Path.cwd() / f"all_data/dataset_extended_ohv_{mode}.data"
    databytes = pickle.dumps(df)
    path.write_bytes(databytes)
    return df


def get_mapa(dataset):
    class_fields = [
        "developer",
        "publisher",
        "platforms",
        "categories",
        "genres",
        "tags",
        "short_description",
        "rating"]
    separate = {"platforms", "categories", "genres", "tags"}
    """
    Ok:
    genres
    categoreis
    tags

    """
    mapa = generate_uniques_map(
        dataset,
        class_fields,
        separate)
    return mapa, class_fields, separate


def save_df(df_final):
    databytes = pickle.dumps(df_final)
    path = Path.cwd() / "all_data/dataset_extended_final.data"
    path.write_bytes(databytes)


@app.command()
def run(n: int = -1, with_nlp: bool = False):
    # cargar dataset original
    dataset = load_train()
    # seleccionar las columnas no numericas para analisis y conversion
    """
     Ok:
     genres
     categoreis
     tags




     """
    nn = len(dataset)
    print(f"Dataset de largo {nn}")
    ones = np.ones(nn)
    dataset["train"] = ones

    mapa, class_fields, separate = get_mapa(dataset)
    # Se crean nuevas columnas que
    # de manera que se identifique el campo según la 'performance'
    # de manera independiente o agrupada en el campo

    # filtrar hasta la fecha
    dataset_sorted = dataset.sort_values(["release_date"])

    # cut a number to test
    if n > 0:
        dataset_sorted = dataset_sorted.head(n)

    # obtener la fecha inicial
    STR_START_DATE = dataset_sorted.iloc[[0]].release_date.item()
    START_DATE = datetime.strptime(STR_START_DATE, FORMAT)

    # integrarte final results with total_games
    print("New dataset", len(dataset_sorted))
    # analisis short_short_description

    # analisis datos no texto

    # + START_ANALISIS_DATO:
    # dataset_sorted = dataset_sorted.sample(n=20)
    dfoh = df_one_hots(dataset_sorted, mapa)
    # create_one_hot_alias(dataset_sorted, mapa)

    df_data = process_data(dataset_sorted, START_DATE, mapa, class_fields)

    # + END_ANALISIS_DATO:
    df_final = pd.merge(df_data, dfoh, left_on="name", right_on="name")

    if with_nlp:
        description, rbag = do_nlp(dataset_sorted)
        df_nlp = build_nlp_dataframe(description, rbag)
        save_nlp(description, rbag)
        df_final = pd.merge(
            df_final, df_nlp, left_index=True, right_index=True)

    dataset_sorted['release_date'] = pd.to_datetime(
        dataset_sorted['release_date'], utc=True)

    df_final_ext = dataset_sorted.merge(
        df_final,
        how='inner', left_index=True, right_on="index_x")
    if n <= 100:
        print(mapa)
        print(df_final_ext.head())
    save_df(df_final_ext)


if __name__ == '__main__':
    app()
