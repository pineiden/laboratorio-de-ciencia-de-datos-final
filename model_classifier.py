from functools import reduce
import pickle
from pathlib import Path
from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingGridSearchCV
from sklearn.feature_selection import (SelectPercentile, f_classif,
                                       mutual_info_classif)

from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.dummy import DummyRegressor
from sklearn.dummy import DummyClassifier
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.compose import ColumnTransformer
from sklearn.metrics import f1_score
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.feature_selection import VarianceThreshold
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from analisis_datos import load_train, get_mapa, FORMAT, load_test
from read_total_extended import get_dataframe_extended, get_dataframe_test_extended
from datetime import datetime

random_state = 128


def drop_non_numeric(dataset):
    fields = list(({"name", "name_x", "name_y"} | set(
        class_fields)) & set(dataset.columns))
    fields.sort()
    result = dataset.drop(fields, axis=1)
    return result


def drop_endings(dataset):
    endings = ("_x", "_y", "_year")
    cols = []
    for c in dataset.columns:
        if any([c.endswith(f) for f in endings]):
            cols.append(c)
    return dataset.drop(cols, axis=1)


class OnlyNumericTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        result = drop_non_numeric(X)
        result = drop_endings(result)
        return result


def regressor_separate_x_y(dataframe):
    y_names = "estimated_sells"
    columns = set(dataframe.columns)
    x_cols = columns - {y_names}
    y = dataframe.drop(list(x_cols), axis=1)
    X = dataframe.drop(list([y_names]), axis=1)
    return X, y


def classifier_separate_x_y(dataframe):
    y_names = "uniques-rating"
    columns = list(dataframe.columns)
    rating = set([c for c in columns if y_names in c])

    others = set(columns) - rating
    y = dataframe.drop(list(others), axis=1)
    X = dataframe.drop(list(rating), axis=1)
    return X, y


def regresion_gamedata(dataframe):
    # excluir = ("ventas", "estimaded_sells")
    columnas = dataframe.columns
    lista_exclusion = [n for n in columnas if "rating" in n]
    return dataframe.drop(lista_exclusion, axis=1)


def clasificador_gamedata(dataframe):
    # excluir = ("ventas", "estimaded_sells")
    columnas = dataframe.columns
    lista_exclusion = [*[n for n in columnas if "ventas" in n],
                       *[n for n in columnas if "estimated_sells" in n]]
    fields = list(set(lista_exclusion) & set(dataframe.columns))
    return dataframe.drop(fields, axis=1)


def date_to_cols(dataset):
    df = dataset.copy()
    df['release_date'] = pd.to_datetime(
        df['release_date'], utc=True)
    df["year"] = pd.DatetimeIndex(df["release_date"]).year
    df["month"] = pd.DatetimeIndex(df["release_date"]).month
    df["day"] = pd.DatetimeIndex(df["release_date"]).day
    return df


class ConvertDatetime2Timestamp(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        result = X.copy()
        field = "release_date"
        fields = [c for c in result.columns if c.startswith(field)]
        if field in result.columns:
            result["ts_release_date"] = result["release_date"].values.astype(
                np.int64) // 10**9
        if f"{field}_x" in result.columns:
            result["ts_release_date"] = result["release_date_x"].values.astype(
                np.int64) // 10**9

        result = result.drop(fields, axis=1)
        return result


class GamedataColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        result = clasificador_gamedata(X)
        return result


class RegressionColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return regresion_gamedata(X)


class DropZeroColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X[~(X == 0).all(axis=1)]


class ExpandDatetimeColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return date_to_cols(X)


def numeric_columns(dataframe, class_fields):
    columnas = dataframe.columns
    aceptar = set(columnas) - set(class_fields)
    return aceptar


def train_and_evaluate(pipe,  X_train, y_train, X_test, y_test, show=True):
    pipe.fit(X_train, y_train)
    y_pred = pipe.predict(X_test)
    if show:
        print("Matriz de confusión")
        print(confusion_matrix(y_test, y_pred, labels=pipe.classes_))
        # print("Reporte clasificación")
        # print(classification_report(y_test,y_pred, target_names=pipe.classes_))
    return f1_score(y_test, y_pred, average="macro")


def toint(vc):
    ones = np.array([i for i, c in enumerate(vc) if c == 1])
    exp = 2**ones
    total = reduce(lambda a, b: a | b, exp)
    return total


if __name__ == "__main__":
    df_train = load_train()
    mapa, class_fields, separate = get_mapa(df_train)

    dataset_extended_final = get_dataframe_extended()

    dataset_test_extended_final = get_dataframe_extended()

    # campos con 'unique' son clases pasadas a onehot
    one_hot_fields = []
    for c in dataset_extended_final.columns:
        if "unique" in c:
            one_hot_fields.append(c)
        if c.startswith("shd"):
            one_hot_fields.append(c)

    numeric = numeric_columns(dataset_extended_final, class_fields)
    numeric_not_one_hot = numeric - set(one_hot_fields)
    columnas = dataset_extended_final.columns
    lista_exclusion = [*[n for n in columnas if "ventas" in n],
                       *[n for n in columnas if "estimated_sells" in n]]
    fields_ventas = set(lista_exclusion)
    numeric_not_one_hot_classifier = numeric_not_one_hot - fields_ventas

    X_mod, y_mod = classifier_separate_x_y(dataset_extended_final)

    lista = [toint(r) for i, r in y_mod.iterrows()]
    y_mod_value = pd.DataFrame(lista)

    X_train, X_test, y_train, y_test = train_test_split(
        X_mod, y_mod_value, test_size=0.33, random_state=random_state, stratify=y_mod_value)
    X_train.rename(columns={"release_date_x": "release_date"}, inplace=True)
    X_test.rename(columns={"release_date_x": "release_date"}, inplace=True)

    column_scaler = ColumnTransformer([
        ("tipicos", StandardScaler(), numeric_not_one_hot_classifier),
        ("one_hot", Normalizer(), one_hot_fields),
    ], remainder="passthrough")

    pca = PCA(svd_solver='full')

    steps_classifier = [
        # ("datetime_expand", ExpandDatetimeColumnTransformer()),
        ("clear_sells", GamedataColumnTransformer()),
        ("convert_release_date", ConvertDatetime2Timestamp()),
        ("drop_non_numeric", OnlyNumericTransformer()),
        ("filter_zeros", DropZeroColumnTransformer()),
        ("imputer", SimpleImputer(strategy="median")),
        ("scaler", Normalizer()),
        ("principal_components", pca),
    ]

    list_classifiers = [
        # ("knn", KNeighborsClassifier()),
        # ("svc", SVC(kernel="poly", C=0.025)),
        # ("svc2",SVC(gamma=1, C=1)),
        ("gaussian", GaussianProcessClassifier(1.0 * RBF(1.0))),
        ("mlpc", MLPClassifier(random_state=random_state, alpha=1, max_iter=2500)),
        # ("adaboost", AdaBoostClassifier()),
        # ("gaussian_nb",GaussianNB()),
        ("random_forest", RandomForestClassifier(
            max_depth=5, n_estimators=10, max_features=5)),
    ]

    classifiers = {}

    for name_clf, clf in list_classifiers:
        pipeline_classifier = Pipeline(
            steps=[
                *steps_classifier,
                ("constant_columns", VarianceThreshold(threshold=0.0)),
                ("selection", SelectPercentile(mutual_info_classif, percentile=20)),
                (f"classifier_{name_clf}", clf)
            ]
        )
        classifiers[name_clf] = pipeline_classifier

    # grilla de parametros
    steps = 256
    start = 30
    end = 100
    step = 2
    n_jobs = 32

    param_grid = {
        "random_forest": [
            {
                "selection__percentile": range(start, end+1, step),
                "selection__score_func": (f_classif, mutual_info_classif)
            },
            # {"principal_components__n_components":range(6,800,5)},
            {
                # ,range(3,16,2),
                "classifier_random_forest__max_depth": [3, 10, 15, 20, 25, 30, 35, 50, 60],
                "classifier_random_forest__max_features": range(4, 12, 2),
                "classifier_random_forest__n_estimators": range(100, 110, 5),
                "classifier_random_forest__criterion": ("gini", "entropy", "log_loss"),
                "classifier_random_forest__min_samples_leaf": range(1, 3),
                "classifier_random_forest__max_leaf_nodes": range(5, 21, 5),
            },
        ],
        "adaboost": [
            {
                "selection__percentile": range(start, end+1, step),
                "selection__score_func": (f_classif, mutual_info_classif)
            },
            {
                "classifier_adaboost__n_estimators": range(300, 500, 20),
                "classifier_adaboost__learning_rate": (.9, 1.1, 1.5),
                # "classifier_adaboost__estimator": [
                #     DecisionTreeClassifier(random_state=random_state,
                #                            max_depth=depth) for depth in range(5, 21, 5)
                # ],
            }
        ],
        "mlpc": [
            {
                "selection__percentile": range(start, end+1, step),
                "selection__score_func": (f_classif, mutual_info_classif)
            },
            {
                "classifier_mlpc__hidden_layer_sizes": [(i,) for i in range(60, 401, 10)],
                "classifier_mlpc__activation": ("identity", "logistic", "tanh", "relu"),
                "classifier_mlpc__learning_rate": ("constant", "invscaling", "adaptive"),
                "classifier_mlpc__solver": ("sgd", "adam"),
            }
        ],
        "gaussian": [
            {
                "selection__percentile": range(start, end+1, step),
                "selection__score_func": (f_classif, mutual_info_classif)
            },
            {
                "classifier_gaussian__kernel": [1.0*RBF(1.0), 1.25*RBF(1.0)]
            }
        ]
    }

    grids = {}

    for name, clf_pipe in classifiers.items():
        grids[f"clf_{name}"] = HalvingGridSearchCV(
            clf_pipe,
            param_grid[name],
            n_jobs=n_jobs,
            scoring="f1_macro")

    y_train_n = y_train.values.ravel()
    y_test_n = y_test.values.ravel()
    best = {name: dict(score={}, params={}, estimators={}, time=0)
            for name in grids}

    print("Entrenamiento con tamaño")
    print(X_train.shape)

    for name, g in grids.items():
        start = datetime.utcnow()
        print(f"Iniciando {name} a {start}")
        score = train_and_evaluate(g,  X_train, y_train_n, X_test, y_test_n)
        end = datetime.utcnow()
        scs = (end-start).total_seconds()
        print(
            f"La inspección del modelo {name} ha durado {scs}, best score {g.best_score_}")
        best[name]["time"] = scs
        best[name]["score"] = g.best_score_
        best[name]["params"] = g.best_params_
        best[name]["estimators"] = g.best_estimator_

    for name, value in best.items():
        print(name, value["score"], value["params"])

    path = Path.cwd() / "models"
    path.mkdir(exist_ok=True)
    now = datetime.utcnow().isoformat()
    for name, values in best.items():
        if values["score"] > .25:
            databytes = pickle.dumps(values)
            pathb = path / f"best_models_{name}_classifiers_{now}.data"
            pathb.write_bytes(databytes)
            print("Guardado modelo en ", pathb)
