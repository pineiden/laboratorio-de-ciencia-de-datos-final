from functools import reduce
from analisis_datos import load_train, get_mapa, FORMAT, load_test
from slugify import slugify
import pandas as pd


def create_one_hot_column(dataset, column, mapa):
    newdataset = []
    for i, (index, data) in enumerate(dataset.iterrows()):
        release_date = data.release_date
        name = data["name"]
        item = {"index": index}
        values = data[column].split(";")
        one_hots = [q for v in values if (q := mapa.get(v))]
        if one_hots:
            all_oh = reduce(lambda a, b: a | b, one_hots)

            fitem = {slugify(f"{column}_{k}"): v for k,
                     v in all_oh.todict().items()}
            item.update(fitem)
        else:
            item.update({slugify(f"{column}"): 0})

        newdataset.append(item)
    df = pd.DataFrame(newdataset)
    # retorna un dataframe cuya cantidad de filaes es
    # la de entrada para train (si se entrena)
    # o la de test si se hace test
    return df


if __name__ == "__main__":
    df_train = load_train()
    mapa, class_fields, separate = get_mapa(df_train)
    df_result = create_one_hot_column(
        df_train, "categories", mapa["uniques_categories"])
    print(df_result.head())
