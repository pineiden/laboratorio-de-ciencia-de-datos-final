# from custom_transformer import GameEstimatorTransformer
from sklearn.svm import SVC
from analisis_datos import load_train, get_mapa, FORMAT, load_test
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from read_total_extended import get_dataframe_extended
from sklearn.compose import ColumnTransformer
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from rich import print
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
from sklearn.base import BaseEstimator, TransformerMixin
from create_one_hot import create_one_hot_column
from model_classifier import ExpandDatetimeColumnTransformer
from datetime import datetime
from sklearn.model_selection import HalvingGridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import (SelectPercentile, f_classif,
                                       mutual_info_classif)
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC
from analisis_datos import do_nlp, build_nlp_dataframe
from custom_transformer import GameEstimatorTransformer
from pathlib import Path
import pickle
from collections import Counter

random_state = 128


def train_and_evaluate(pipe,  X_train, y_train, X_test, y_test, show=True):
    pipe.fit(X_train, y_train)
    y_pred = pipe.predict(X_test)
    if show:
        print("Matriz de confusión")
        print(confusion_matrix(y_test, y_pred, labels=pipe.classes_))
        # print("Reporte clasificación")
        # print(classification_report(y_test,y_pred, target_names=pipe.classes_))
    return f1_score(y_test, y_pred, average="macro")


class ShortDescriptionNLPColumnTransform(BaseEstimator, TransformerMixin):

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):

        description, rbag = do_nlp(X)
        dataframe_nlp = build_nlp_dataframe(description, rbag)
        try:
            df = pd.merge(
                X,
                dataframe_nlp,
                left_index=True,
                right_index=True)

            df.rename(columns={"name_x": "name"}, inplace=True)
            return df
        except Exception as e:
            print("Error merge", e)
            raise e


class OneHotClassColumnTransform(BaseEstimator, TransformerMixin):
    def __init__(self, mapa):
        self.mapa = mapa

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        columns = X.columns
        for key, mapa in self.mapa.items():
            field = key.replace("uniques_", "")
            if field in columns:
                df = create_one_hot_column(X, field, mapa)
                X = X.merge(df, left_index=True, right_index=True)
        return X


class DropClassFieldsColumnTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, fields):
        self.fields = set(fields) | {"name", "release_date", "name_x",
                                     "name_y"}

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X = X.copy()
        fields = set(X.columns) & self.fields
        result = X.drop(columns=list(fields), axis=1)
        result.fillna(0)
        print("ONEHOT X", X, type(X), X.shape)
        return result


def dataset_classes_one_hot():
    df_train = load_train()
    df_test = load_test()
    mapa, class_fields, separate = get_mapa(df_train)
    dataset_sorted = df_train.sort_values(["release_date"])
    # columns = ["name", "release_date", "short_description", "average_playtime"]
    target = ["rating"]
    select = ["rating", "estimated_sells"]
    X_mod = df_train.drop(select, axis=1)
    y_mod = df_train[select]
    y_target = y_mod[target]
    X_train, X_test, y_train, y_test = train_test_split(
        X_mod, y_target, test_size=0.33, random_state=128, stratify=y_target)

    ohm = OneHotClassColumnTransform(mapa)

    numeric = Pipeline(
        [
            ('escala_normal', StandardScaler()),
            ("pca", PCA()),
        ])

    col_mod = ColumnTransformer(
        [
            ("numeric", numeric, ["average_playtime", "year", "month",
                                  "day", "english", "achievements", "price"]),
            ("class2onehot", ohm, []),
        ]
    )
    # # final ada
    dt = DecisionTreeClassifier(max_depth=1)
    # sgd = SGDClassifier()
    svc = SVC(probability=True, kernel='linear')
    # lin_svc = LinearSVC(random_state=128, tol=1e-5)
    pipeline_ada = Pipeline([
        ("datetime_expand", ExpandDatetimeColumnTransformer()),
        ("transform", col_mod),
        ("selection", SelectPercentile(mutual_info_classif, percentile=20)),
        ("classifier_adaboost", AdaBoostClassifier(svc,
                                                   random_state=128, algorithm="SAMME"))
        # ("classifier_mlpc", MLPClassifier(
        #    random_state=128, alpha=1, max_iter=2500)),
    ])
    y_train_n = y_train.values.ravel()
    y_test_n = y_test.values.ravel()

    pipeline_ada.fit(X_train, y_train_n)
    y_pred = pipeline_ada.predict(X_test)
    score = f1_score(y_pred, y_test, average="weighted")
    print("Score f1 ada", score)

    print("========")
    params_ada = {c: v for c, v in pipeline_ada.get_params().items()
                  if "adaboost" in c}
    print(params_ada)
    print("========")

    return {"X": X_mod, "y": y_mod}, pipeline_ada


def adding_nlp_analysis():
    df_train = load_train()
    n = 500
    df_train = df_train.head(n)
    df_test = load_test()
    mapa, class_fields, separate = get_mapa(df_train)

    dataset_sorted = df_train.sort_values(["release_date"])
    # columns = ["name", "release_date", "short_description", "average_playtime"]
    target = ["rating"]
    select = ["rating", "estimated_sells"]
    X_mod = df_train.drop(select, axis=1)
    y_mod = df_train[select]
    y_target = y_mod[target]
    X_train, X_test, y_train, y_test = train_test_split(
        X_mod, y_target, test_size=0.33, random_state=128, stratify=y_target)

    ohm = OneHotClassColumnTransform(mapa)

    numeric = Pipeline(
        [
            ('escala_normal', StandardScaler()),
            ("pca", PCA()),
        ])

    col_mod = ColumnTransformer(
        [
            ("numeric", numeric, ["average_playtime", "year", "month",
                                  "day", "english", "achievements", "price"]),
            ("class2onehot", ohm, []),
        ]
    )
    # # final ada
    dt = DecisionTreeClassifier(max_depth=1)
    # sgd = SGDClassifier()
    svc = SVC(probability=True, kernel='linear')
    # lin_svc = LinearSVC(random_state=128, tol=1e-5)
    pipeline_ada = Pipeline([
        ("datetime_expand", ExpandDatetimeColumnTransformer()),
        ("short_description_nlp", ShortDescriptionNLPColumnTransform()),
        ("transform", col_mod),
        ("selection", SelectPercentile(mutual_info_classif, percentile=60)),
        ("classifier_adaboost", AdaBoostClassifier(svc,
                                                   random_state=128, algorithm="SAMME"))
        # ("classifier_mlpc", MLPClassifier(
        #    random_state=128, alpha=1, max_iter=2500)),
    ])
    y_train_n = y_train.values.ravel()
    y_test_n = y_test.values.ravel()

    pipeline_ada.fit(X_train, y_train_n)
    y_pred = pipeline_ada.predict(X_test)
    score = f1_score(y_pred, y_test, average="weighted")

    print("Score f1 ada", score)

    print("========")

    params_ada = {c: v for c, v in pipeline_ada.get_params().items()
                  if "adaboost" in c}
    print(params_ada)
    print("========")

    y_pred = pipeline_ada.predict(df_test)
    # final training ... is ok, then train with more data
    # X_train, X_test, y_train, y_test = train_test_split(
    #     X_mod, y_target, test_size=0.05, random_state=128, stratify=y_target)
    # y_train_n = y_train.values.ravel()
    # y_test_n = y_test.values.ravel()
    # print(X_train.shape)
    # pipeline_ada.fit(X_train, y_train_n)

    # db_test
    params_ada.update({"evaluation": y_pred})
    print("Resultados evaluacion")

    path = Path("predictions_noprestigio_clf.txt")

    with path.open("w", encoding="utf8") as f:
        for i, v in enumerate(y_pred):
            f.write(f"{v}\n")
            data = df_test.iloc[i]
            print(data["name"], v)

    return params_ada, pipeline_ada


def do_prestigio():
    df_train = load_train()
    #df_train = df_train.head(100)
    print(df_train.info())
    df_test = load_test()
    mapa, class_fields, separate = get_mapa(df_train)
    print("Class fields", class_fields)

    reverse_map = {}
    for n, v in mapa["uniques_rating"].items():
        reverse_map[v.toint] = n

    dataset_sorted = df_train.sort_values(["release_date"])
    # columns = ["name", "release_date", "short_description", "average_playtime"]
    target = ["rating"]
    select = ["rating", "estimated_sells"]
    X_mod = df_train.drop(select, axis=1)

    print("TRAIN SHAPE", X_mod.shape)
    print("TEST EVALUATION", df_test.shape)

    y_mod = df_train[select]
    y_target = y_mod[target]
    X_train, X_test, y_train, y_test = train_test_split(
        X_mod, y_target, test_size=0.25, random_state=128, stratify=y_target)

    print("Train size", X_train.shape)
    ohm = OneHotClassColumnTransform(mapa)
    prestigio = GameEstimatorTransformer(mapa, class_fields, separate)

    numeric = Pipeline(
        [
            ('escala_normal', StandardScaler()),
            ("pca", PCA()),
        ])

    col_mod = ColumnTransformer(
        [
            ("class2onehot", ohm, []),
            ("numeric", numeric, ["average_playtime", "year", "month",
                                  "day", "english", "achievements", "price"]),
        ], n_jobs=-1
    )
    # # final ada
    dt = DecisionTreeClassifier(max_depth=1)
    # sgd = SGDClassifier()
    svc = SVC(probability=True, kernel='linear')
    # lin_svc = LinearSVC(random_state=128, tol=1e-5)
    adaboost = AdaBoostClassifier(dt,
                                  n_estimators=50,
                                  random_state=128,
                                  algorithm="SAMME.R")
    pipeline_ada = Pipeline([
        #("short_description_nlp", ShortDescriptionNLPColumnTransform()),
        #("prestigio", prestigio),
        ("datetime_expand", ExpandDatetimeColumnTransformer()),
        ("transform", col_mod),
        #("drop_class_fields", DropClassFieldsColumnTransformer(class_fields)),
        ("selection", SelectPercentile(mutual_info_classif, percentile=70)),
        ("classifier_adaboost", adaboost)
    ])
    y_train_n = y_train.values.ravel()
    y_test_n = y_test.values.ravel()

    pipeline_ada.fit(X_train, y_train_n)

    X_test_conv = prestigio.fit_transform(X_test)
    y_pred = pipeline_ada.predict(X_test)
    score = f1_score(y_pred, y_test, average="weighted")
    y_pred_1 = y_pred
    print("Score f1 ada", score)

    print("========")

    params_ada = {c: v for c, v in pipeline_ada.get_params().items()
                  if "adaboost" in c}

    print("Resultados evaluacion - test trained :: Test")
    print(Counter(y_test_n))

    print(params_ada)
    print("========")
    # dataset for evaluation
    prestigio.switch_mode()
    y_pred = pipeline_ada.predict(df_test)
    # final training ... is ok, then train with more data
    # X_train, X_test, y_train, y_test = train_test_split(
    #     X_mod, y_target, test_size=0.05, random_state=128, stratify=y_target)
    # y_train_n = y_train.values.ravel()
    # y_test_n = y_test.values.ravel()
    # print(X_train.shape)
    # pipeline_ada.fit(X_train, y_train_n)

    # db_test
    # params_ada.update(
    #     {"evaluation": y_pred, "importance": adaboost.feature_importances_})

    print("Score f1 ada", score)

    print("Resultados evaluacion - Y :: Train")
    print(Counter(y_train_n))

    print("Resultados evaluacion - Y :: Test")
    print(Counter(y_test_n))

    print("Resultados test vector")
    print(Counter(y_pred_1))

    print("Resultados evaluacion")
    print(Counter(y_pred))

    path = Path("dt_simple_predictions_clf.txt")

    # print("feature importance",  adaboost.feature_importances_)
    with path.open("w", encoding="utf8") as f:
        for i, v in enumerate(y_pred):
            f.write(f"{v}\n")
            data = df_test.iloc[i]
            # print(data["name"], v)
    return params_ada, pipeline_ada


def proceso_iterativo():
    opts = {
        # "dc_onehot": dataset_classes_one_hot,
        # "do_nlp": adding_nlp_analysis,
        "do_prestigio": do_prestigio
    }
    models = []
    datasets = []
    names = []
    for n, callback in opts.items():
        names .append(n)
        dataset, model = callback()
        if dataset:
            datasets.append(dataset)
        if model:
            models.append(model)

    path = Path.cwd() / "models"
    path.mkdir(exist_ok=True)
    now = datetime.utcnow().isoformat()
    for name, model, dataset in zip(names, models, datasets):
        databytes = pickle.dumps({"model": model, "dataset": dataset})
        pathb = path / f"best_models_{name}_classifiers_{now}.data"
        pathb.write_bytes(databytes)
        print("Guardado modelo en ", pathb)

    return datasets, models


if __name__ == "__main__":
    proceso_iterativo()
