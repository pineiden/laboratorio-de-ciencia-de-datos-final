from analisis_datos import load_train, get_mapa, FORMAT, load_test
import sys
from datetime import datetime, timedelta
import numpy as np
from custom_transformer import GameEstimatorTransformer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import train_test_split
from read_total_extended import get_dataframe_extended
from sklearn.dummy import DummyClassifier
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.compose import ColumnTransformer
from sklearn.metrics import f1_score
from sklearn.metrics import r2_score


def clasificador_gamedata(dataframe):
    #excluir = ("ventas", "estimaded_sells")
    columnas = dataframe.columns
    lista_exclusion = [*[n for n in columnas if "ventas" in n],
                       *[n for n in columnas if "estimated_sells" in n]]
    fields = list(set(lista_exclusion) & set(dataframe.columns))
    return dataframe.drop(fields, axis=1)


def regresion_gamedata(dataframe):
    #excluir = ("ventas", "estimaded_sells")
    columnas = dataframe.columns
    lista_exclusion = [n for n in columnas if "rating" in n]
    return dataframe.drop(lista_exclusion, axis=1)


def separate_x_y(dataset):
    y_names = "uniques-rating"
    columns = list(dataset.columns)
    rating = set([c for c in columns if y_names in c])
    others = set(columns) - rating
    y = dataset[list[rating]]
    X = dataset[list[others]]
    return X, y


def drop_non_numeric(dataset, class_fields):
    fields = list(({"name", "name_x", "name_y"} | set(
        class_fields)) & set(dataset.columns))
    fields.sort()
    result = dataset.drop(fields, axis=1)
    return result


def numeric_columns(dataframe, class_fields):
    columnas = dataframe.columns
    aceptar = set(columnas) - set(class_fields)
    return aceptar


class OnlyNumericTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        print("Only numeric trs")
        result = drop_non_numeric(X)
        print("END Only numeric trs")
        return result


class ConvertDatetime2Timestamp(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        result = X.copy()
        field = "release_date"
        fields = [c for c in result.columns if c.startswith(field)]
        if field in result.columns:
            result["ts_release_date"] = result["release_date"].values.astype(
                np.int64) // 10**9
        if f"{field}_x" in result.columns:
            result["ts_release_date"] = result["release_date_x"].values.astype(
                np.int64) // 10**9

        result = result.drop(fields, axis=1)
        return result


class GamedataColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        print("GamedataColumnTransformer-> filtrando")
        result = clasificador_gamedata(X)
        print("GamedataCT retornando", result.columns,
              'developer_1980s_ventas_med_month' in result.columns)
        return result


class RegressionColumnTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return regresion_gamedata(X)


if __name__ == "__main__":
    df_train = load_train()
    mapa, class_fields, separate = get_mapa(df_train)
    dataset_extended_final = get_dataframe_extended()
    X_mod, y_mod = separate_x_y(dataset_extended_final)
    # campos con 'unique' son clases pasadas a onehot
    one_hot_fields = []
    for c in dataset_extended_final.columns:
        if "unique" in c:
            one_hot_fields.append(c)
        if c.startswith("shd"):
            one_hot_fields.append(c)

    numeric = numeric_columns(X, class_fields)
    numeric_not_one_hot = numeric - set(one_hot_fields)
    columnas = dataset_extended_final.columns
    lista_exclusion = [*[n for n in columnas if "ventas" in n],
                       *[n for n in columnas if "estimated_sells" in n]]
    fields_ventas = set(lista_exclusion)
    numeric_not_one_hot_classifier = numeric_not_one_hot - fields_ventas

    # PCA set to 1024
    pca_1024 = PCA(n_components=1024, svd_solver='full')
    # está ok
    numeric_pipeline = Pipeline(
        steps=[
            ("convert_release_date", ConvertDatetime2Timestamp()),
            ("drop_non_numeric", OnlyNumericTransformer()),
            ("imputer", SimpleImputer(strategy="median")),
            ("scaler", Normalizer())
        ]
    )

    # revisamos esto
    numeric_transformer = ColumnTransformer(
        transformers=[
            ("numeric_pipeline", numeric_pipeline, numeric_not_one_hot_classifier)
        ]
    )

    steps_classifier = [
        ("clear_sells", GamedataColumnTransformer()),
        ("numeric_transformer", numeric_transformer),  # retorn un numpy array
        ("principal_components", pca_1024),
    ]

    dummy_pipeline_classifier = Pipeline(
        steps=[
            *steps_classifier,
            ("classifier", DummyClassifier())
        ]
    )

    steps_regression = [
        ("clear_rating", RegressionColumnTransformer()),
        ("numeric_transformer", numeric_transformer),
        ("principal_components", pca_1024),
    ]

    dummy_pipeline_regression = Pipeline(
        steps=[
            *steps_regression,
            ("classifier", DummyClassifier())
        ]
    )

    print(dummy_pipeline_classifier)
