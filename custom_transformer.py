from sklearn.base import BaseEstimator, TransformerMixin
from analisis_datos import (
    get_mapa,
    df_one_hots,
    process_data,
    do_nlp,
    FORMAT,
    create_one_hot_alias,
    build_nlp_dataframe)
from datetime import datetime, timedelta
import pickle
import pandas as pd
from pathlib import Path
import numpy as np
import pytz


def localize(x):
    return pytz.utc.localize(x)


class GameEstimatorTransformer(BaseEstimator, TransformerMixin):
    def __init__(self,  mapa, class_fields, separate, train=True):
        self.train = train
        self.mapa = mapa
        self.class_fields = class_fields
        self.separate = separate
        self.X = pd.DataFrame([])
        self.bag = set()

    def switch_mode(self):
        self.train = not self.train

    @property
    def mode(self):
        return "train" if self.train else "test"

    def fit(self, X, y=None):
        return self

    def set_x(self, X):
        if self.mode == "train":
            n = len(X)
            ones = np.ones(n)
            X["train"] = ones
            X['release_date'] = pd.to_datetime(X['release_date'], utc=True)

            if not self.X.empty:
                before = self.X
                self.X = pd.concat((before, X), axis=0).\
                    drop_duplicates().\
                    sort_values(["release_date"])
            else:
                self.X = X

    def transform(self, X, y=None):

        # dataset es el df que entra : X
        dataset = X
        # en caso de estar en modo train
        # se agrega a lo que ya existe
        if self.mode == "train":
            self.set_x(X)
        else:
            # print("ADDDING NEW DATSTE TEST", set(
            #     self.X.columns)-set(dataset.columns))
            future = datetime.utcnow() + timedelta(days=30)
            dataset["release_date"] = localize(future)  # .strftime("%Y-%m-%d")
            # en caso de hacer test o prod
            # se agregan 0 a dataset entrante
            dataset["train"] = 0
            # se concatena historico de train con nuevo dataset
            # juntar en la vertical
            dataset = pd.concat((self.X, dataset), axis=0)

        # seleccionar las columnas no numericas para analisis y conversion
        """
        Ok:
        genres
        categoreis
        tags

        """
        # se ordenan por fecha de publicación
        dataset_sorted = dataset.sort_values(["release_date"])
        # if self.mode == "test":
        #     test_filter = dataset_sorted["train"] == 0.0
        # dataset_sorted = dataset_sorted.where(test_filter, other="Mixed")
        # Se crean nuevas columnas que
        # de manera que se identifique el campo según la 'performance'
        # de manera independiente o agrupada en el campo

        # filtrar hasta la fecha

        # obtener la fecha inicial tomado del dataset entrante

        # integrarte final results with total_games
        # analisis short_short_description

        # analisis datos no texto

        # + START_ANALISIS_DATO:
        # dataset_sorted = dataset_sorted.sample(n=20)
        # cada campo clase pasará a ser un onehot

        # print("A SORTED DATSET FULL", dataset_sorted.shape)

        # dfoh = create_one_hot_alias(dataset_sorted,
        #                             self.mapa,
        #                             train=self.train)

        # print("===================")
        # print("Columnas one hot")
        # print("uniques-categories-captions-available" in dfoh.columns)
        # print("===================")
        # breakpoint()
        # create_one_hot_alias(dataset_sorted, mapa)
        dataset_sorted['release_date'] = pd.to_datetime(
            dataset_sorted['release_date'], utc=True)

        dt = dataset_sorted.iloc[[0]].release_date.item()
        START_DATE = dt  # datetime.strptime(STR_START_DATE, FORMAT)

        # esta función tomará todos los datos
        # extraerá los valores históricos correspondientes
        # al data row que se está estudiando
        # debe discriminar si está en test o train
        df_data = process_data(dataset_sorted,
                               START_DATE,
                               self.mapa,
                               self.class_fields,
                               train=self.train)

        # start df final
        # df_final = pd.merge(df_data, dfoh, left_on="name", right_on="name")

        # retornará el dataset de tamaño igual
        # ala data de entrada correspindiente (train/test)

        # no_train_filter = "train == 0"
        # dataset_sorted_select = dataset_sorted.query(no_train_filter)
        # # esto junta los dataframes según el mismo nombre

        # df_nlp = self.do_nlp(dataset_sorted_select)
        # + END_ANALISIS_DATO:
        # esto junta los dataframes según el mismo nombre

        # se hace una analisis de short_description
        # usando lenguaje natural, obteniendo valores en base a su estructura
        # segunda juntura
        # df_final = pd.merge(
        #     df_final, df_nlp, left_index=True, right_index=True)

        # final final final

        if self.mode == "train":
            df_final_ext = pd.merge(
                dataset_sorted, df_data,
                left_on="name",
                right_on="name")

            df_final_ext.rename(columns={"name_x": "name"}, inplace=True)
            # Perform arbitary transformation

            return df_final_ext
        else:
            df_final_ext = pd.merge(
                dataset_sorted, df_data,
                left_on="name",
                right_on="name")

            df_final_ext.rename(columns={"name_x": "name"}, inplace=True)
            # Perform arbitary transformation
            df_final_ext.fillna(0)
            return df_final_ext

    # def do_nlp(self, dataset_sorted):
    #     description, bag = do_nlp(dataset_sorted, train=self.train)

    #     if self.train:
    #         self.bag = bag
    #     else:
    #         diferencia = set(self.bag) - set(bag)
    #         print("Estas palabras no están registradas", diferencia)

    #     df_nlp = build_nlp_dataframe(description, self.bag)
    #     return df_nlp
