from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class OneHot:
    value: int
    chars: int = 1

    def __str__(self):
        binario = bin(self.value)
        numbers = binario[2::]
        left = self.chars - len(numbers)
        if left < 1:
            left = 1
        return f"{'0'*left}{numbers}"

    def __eq__(self, other):
        return self.value == other.value

    def __ilshift__(self, other):
        cls = type(self)
        assert isinstance(other, cls), "Valor debe ser tipo OneHot"
        return cls(self.value << other.value, max((self.chars, other.chars)))

    def __rlshift__(self, other):
        cls = type(self)
        assert isinstance(other, OneHot), "Valor debe ser tipo OneHot"
        return cls(self.value >> other.value, max((self.chars, other.chars)))

    def __or__(self, other):
        cls = type(self)
        assert isinstance(other, OneHot), "Valor debe ser tipo OneHot"
        return cls(self.value | other.value, max((self.chars, other.chars)))

    def __mul__(self, other):
        cls = type(self)
        assert isinstance(other, OneHot), "Valor debe ser tipo OneHot"
        val = self.value & other.values()
        return cls(val, max(self.chars, other.chars))

    def __and__(self, other):
        cls = type(self)
        assert isinstance(other, OneHot), "Valor debe ser tipo OneHot"
        val = self.value & other.values()
        return cls(val, max(self.chars, other.chars))

    def __len__(self):
        return self.value.bit_count()
