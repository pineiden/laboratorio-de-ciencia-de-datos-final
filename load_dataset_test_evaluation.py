from custom_transformer import GameEstimatorTransformer
from analisis_datos import load_train, get_mapa, FORMAT, load_test
import pickle
from pathlib import Path
import pandas as pd


def save_data_df(df_sorted_1, train=False):
    databytes = pickle.dumps(df_sorted_1)
    mode = "train" if train else "test"
    path = Path.cwd() / f"all_data/dataset_test_extended_{mode}.data"
    path.write_bytes(databytes)
    return df_sorted_1


df_test = load_test()
df_train = load_train()

df_test['release_date'] = pd.to_datetime(df_test['release_date'], utc=True)


dataset_sorted = df_train.sort_values(["release_date"])
mapa, class_fields, separate = get_mapa(df_train)


game_transformer = GameEstimatorTransformer(mapa, class_fields, separate)
print("Set ds base")
game_transformer.set_x(dataset_sorted)

if game_transformer.mode == "train":
    game_transformer.switch_mode()
    print(game_transformer.mode)


df_test['release_date'] = pd.to_datetime(df_test['release_date'], utc=True)


print("Transform")
dataset_test_extended = game_transformer.fit_transform(df_test.head(10))


print(dataset_test_extended)

# save_data_df(dataset_test_extended)
