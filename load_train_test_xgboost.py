#from custom_transformer import GameEstimatorTransformer
from analisis_datos import load_train, get_mapa, FORMAT, load_test
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from read_total_extended import get_dataframe_extended
from sklearn.compose import ColumnTransformer
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from rich import print
from sklearn.decomposition import PCA
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier


dataset_extended_final = get_dataframe_extended()

df_test = load_test()
df_train = load_train()


mapa, class_fields, separate = get_mapa(df_train)
reverse_map = {}
for n, v in mapa["uniques_rating"].items():
    print(n, v, v.toint)
    reverse_map[v.toint] = n
print(mapa["uniques_rating"].keys())

map_target = {n: i for i, n in enumerate(mapa["uniques_rating"])}
dataset_sorted = df_train.sort_values(["release_date"])

#game_transformer = GameEstimatorTransformer()
# game_transformer.set_x(dataset_sorted)

# if game_transformer.mode == "train":
#    game_transformer.switch_mode()
#    print(game_transformer.mode)

#dataset_test_extendeed = game_transformer.fit_transform(df_test)
columns = ["name", "release_date", "short_description", "average_playtime"]
target = ["rating"]


def date_to_cols(dataset):
    df = dataset.copy()
    df['release_date'] = pd.to_datetime(
        df['release_date'], utc=True)
    df["year"] = pd.DatetimeIndex(df["release_date"]).year
    df["month"] = pd.DatetimeIndex(df["release_date"]).month
    df["day"] = pd.DatetimeIndex(df["release_date"]).day
    return df


df_ext_1 = date_to_cols(dataset_sorted)

print(df_ext_1.head())
print(df_ext_1.info())

text_clf = Pipeline(
    [
        ('vect', CountVectorizer(stop_words='english')),
        ('tfidf', TfidfTransformer()),
        # sublinear_tf=True,
        # min_df=5, norm='l2',
        # encoding='latin-1',
        # ngram_range=(1, 2),
        # stop_words='english')),
    ])

numeric = Pipeline(
    [
        ('escala_normal', StandardScaler()),
        ("pca", PCA())
    ])


col_mod = ColumnTransformer(
    [
        ("numeric", numeric, ["average_playtime", "year", "month",
                              "day", "english", "achievements", "price"]),
        # ("nlp", text_clf, ["short_description", "name", "categories", "tags"]),
    ]
)


# final naive bayes
# pipeline_nb = Pipeline([
#     ("transform", col_mod),
#     ("clf_naive_bayes", MultinomialNB())
# ])


# final ada
pipeline_xgboost = Pipeline([
    ("transform", col_mod),
    ("clf_xgb", XGBClassifier(
        learning_rate=.02,
        objective='binary:logistic',
        nthread=64))
])


print(target)
y_mod_pre = df_ext_1.pop("rating")
y_mod = np.array([map_target[v] for v in y_mod_pre])
X_mod = df_ext_1


print(X_mod.shape, y_mod.shape)

X_train, X_test, y_train, y_test = train_test_split(
    X_mod, y_mod, test_size=0.33, random_state=128, stratify=y_mod)


######################
print(X_train.shape)
print(y_train.shape)
#ctc = np.concatenate([X_train, y_train], 1)
# breakpoint()


X_pos = pipeline_xgboost.fit(X_train, y_train)

y_pred = pipeline_xgboost.predict(X_test)

print("XGBOOST", f1_score(y_pred, y_test, average='micro'))


random_state = 128

parameters = {
    "transform__numeric__pca__n_components": range(4, 6, 2),
    # 'vect__ngram_range': [(1, 1), (1, 2)],
    # 'tfidf__use_idf': (True, False),
    # "transform__numeric__escala_normal__norm": ("l1", "l2"),
    # 'clf_naive_bayes__alpha': (1, .5, .1, 1e-2, 1e-3),
    "clf_xgb__n_estimators": [30, 40, 50, 60, 70, 80],
}


gs_clf = GridSearchCV(pipeline_xgboost, parameters, n_jobs=32)
gs_clf = gs_clf.fit(X_train, y_train)

print(gs_clf.best_score_,
      gs_clf.best_params_)
